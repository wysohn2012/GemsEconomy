/*
 * Copyright Xanium Development (c) 2013-2018. All Rights Reserved.
 * Any code contained within this document, and any associated APIs with similar branding
 * are the sole property of Xanium Development. Distribution, reproduction, taking snippets or claiming
 * any contents as your own will break the terms of the license, and void any agreements with you, the third party.
 * Thank you.
 */

package me.xanium.gemseconomy.account;

import me.xanium.gemseconomy.GemsEconomy;
import me.xanium.gemseconomy.common.Manager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.Optional;

public class AccountManager extends Manager<Account> {

    public AccountManager(GemsEconomy plugin) {
        super(plugin);
    }

    public Account get(Player player) {
        return get(player.getUniqueId());
    }

    public Account get(String name) {
        return Optional.of(name)
                .map(Bukkit::getOfflinePlayer) //TODO probably it uses internet access
                .map(OfflinePlayer::getUniqueId)
                .map(this::get)
                .orElse(null);
    }


    //    public List<Account> getAllAccounts() {
//        //TODO this is I/O blocking call. Should not be called by server thread.
//        //Best way is to use callback
//
//        return plugin.getDataStore().getOfflineAccounts();
//    }
}

