/*
 * Copyright Xanium Development (c) 2013-2018. All Rights Reserved.
 * Any code contained within this document, and any associated APIs with similar branding
 * are the sole property of Xanium Development. Distribution, reproduction, taking snippets or claiming
 * any contents as your own will break the terms of the license, and void any agreements with you, the third party.
 * Thank you.
 */

package me.xanium.gemseconomy.listeners;

import me.xanium.gemseconomy.GemsEconomy;
import me.xanium.gemseconomy.account.Account;
import me.xanium.gemseconomy.file.F;
import me.xanium.gemseconomy.utils.UtilServer;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Optional;
import java.util.UUID;

public class EconomyListener implements Listener {

    private final GemsEconomy plugin = GemsEconomy.getInstance();

    private Account newPlayer(UUID uniqueId, String name) {
        Account account;
        account = new Account(uniqueId, name);

        plugin.getDataStore().createAccount(account);
        plugin.getDataStore().saveAccount(account);

        UtilServer.consoleLog("New Account created for: " + account.getDisplayName());
        return account;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onLogin(AsyncPlayerPreLoginEvent event) {
        // this event itself is async. Do not spawn a new async inside the async call.
        // Also, it can block the users from joining before necessary jobs are done

        // check if save data exist
        Account account = plugin.getDataStore().loadAccount(event.getUniqueId());

        if (account == null) { // this is new user
            account = newPlayer(event.getUniqueId(), event.getName());
        } else { // existing user
            account.setNickname(event.getName());
        }

        plugin.getAccountManager().add(account);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        Account account = plugin.getAccountManager().get(player.getUniqueId());

        // sometimes, AsyncPlayerPreLoginEvent is skipped and PlayerJoinEvent is called straight
        // at this point, we have no choice but to block the main thread and load
        if (account == null) {
            account = plugin.getDataStore().loadAccount(player.getUniqueId());

            if (account == null) { // this is new user
                account = newPlayer(player.getUniqueId(), player.getName());
            } else { // existing user
                account.setNickname(player.getName());
            }
        }

        plugin.getAccountManager().add(account);

        Optional.of(plugin)
                .map(JavaPlugin::getServer)
                .map(Server::getScheduler)
                .ifPresent(bukkitScheduler -> bukkitScheduler.runTaskLater(plugin, () -> {
                    if (plugin.getCurrencyManager().getDefaultCurrency() == null && (player.isOp() || player.hasPermission("gemseconomy.command.currency"))) {
                        player.sendMessage(F.getPrefix() + "§cYou have not made a currency yet. Please do so by \"§e/currency§c\".");
                    }
                }, 60L));
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        plugin.getAccountManager().remove(player.getUniqueId());
    }
}

