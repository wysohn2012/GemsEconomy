/*
 * Copyright Xanium Development (c) 2013-2018. All Rights Reserved.
 * Any code contained within this document, and any associated APIs with similar branding
 * are the sole property of Xanium Development. Distribution, reproduction, taking snippets or claiming
 * any contents as your own will break the terms of the license, and void any agreements with you, the third party.
 * Thank you.
 */
package me.xanium.gemseconomy.data;

import me.xanium.gemseconomy.account.Account;
import me.xanium.gemseconomy.currency.Currency;
import me.xanium.gemseconomy.utils.UtilServer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class YamlStorage extends DataStore {
    private final File file;
    private YamlConfiguration configuration;

    public YamlStorage(File file) {
        super("YAML", false);
        this.file = file;
    }

    @Override
    public void initialize() {
        if (!getFile().exists()) {
            try {
                if (getFile().createNewFile()) {
                    UtilServer.consoleLog("Data file created.");
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        configuration = new YamlConfiguration();
        try {
            configuration.load(getFile());
        } catch (IOException | InvalidConfigurationException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void close() {

    }

    @Override
    public void loadCurrencies() {
        ConfigurationSection section = getConfig().getConfigurationSection("currencies");
        if (section != null) {
            Set<String> currencies = section.getKeys(false);
            for (String uuid : currencies) {
                String path = toPath("currencies", uuid);
                String single = getConfig().getString(toPath(path, "singular"));
                String plural = getConfig().getString(toPath(path, "plural"));
                Currency currency = new Currency(UUID.fromString(uuid), single, plural);
                currency.setColor(ChatColor.valueOf(getConfig().getString(toPath(path, "color")).toUpperCase()));
                currency.setDecimalSupported(getConfig().getBoolean(toPath(path, "decimalsupported")));
                currency.setDefaultBalance(getConfig().getDouble(toPath(path, "defaultbalance")));
                currency.setDefaultCurrency(getConfig().getBoolean(toPath(path, "defaultcurrency")));
                currency.setPayable(getConfig().getBoolean(toPath(path, "payable")));
                currency.setSymbol(getConfig().getString(toPath(path, "symbol")));
                currency.setExchangeRate(getConfig().getDouble(toPath(path, "exchange_rate")));
                plugin.getCurrencyManager().add(currency);
                UtilServer.consoleLog("Loaded currency: " + currency.getSingular());
            }
        }
    }

    @Override
    public void updateCurrencyLocally(Currency currency) {
        throw new UnsupportedOperationException("This method is not supported in SQLite.");
    }

    @Override
    public void saveCurrency(Currency currency) {
        String path = toPath("currencies", currency.getUuid());
        getConfig().set(toPath(path, "singular"), currency.getSingular());
        getConfig().set(toPath(path, "plural"), currency.getPlural());
        getConfig().set(toPath(path, "defaultbalance"), currency.getDefaultBalance());
        getConfig().set(toPath(path, "symbol"), currency.getSymbol());
        getConfig().set(toPath(path, "decimalsupported"), currency.isDecimalSupported());
        getConfig().set(toPath(path, "defaultcurrency"), currency.isDefaultCurrency());
        getConfig().set(toPath(path, "payable"), currency.isPayable());
        getConfig().set(toPath(path, "color"), currency.getColor().name());
        getConfig().set(toPath(path, "exchange_rate"), currency.getExchangeRate());
        saveCurrentState();
    }

    @Override
    public void deleteCurrency(Currency currency) {
        String path = toPath("currencies", currency.getUuid());
        getConfig().set(path, null);
        saveCurrentState();
    }

    @Override
    public Map<String, Double> getTopList(Currency currency, int offset, int amount) {
        return null;
    }

    @Override
    public Account loadAccount(String name) {
        ConfigurationSection section = getConfig().getConfigurationSection("accounts");
        if (section != null) {
            Set<String> accounts = section.getKeys(false);
            if (!accounts.isEmpty()) {
                for (String uuid : accounts) {
                    String path = toPath("accounts", uuid);
                    String nick = getConfig().getString(toPath(path, "nickname"));
                    if (nick != null && nick.equalsIgnoreCase(name)) {
                        Account account = new Account(UUID.fromString(uuid), nick);
                        account.setCanReceiveCurrency(getConfig().getBoolean(toPath(path, "payable")));
                        loadBalances(account);
                        plugin.getAccountManager().add(account);
                        return account;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public Account loadAccount(UUID uuid) {
        String path = toPath("accounts", uuid);
        String nick = getConfig().getString(toPath(path, "nickname"));
        if (nick != null) {
            Account account = new Account(uuid, nick);
            account.setCanReceiveCurrency(getConfig().getBoolean(toPath(path, "payable")));
            loadBalances(account);
            plugin.getAccountManager().add(account);
            return account;
        }
        return null;
    }

    @Override
    public void saveAccount(Account account) {
        String path = toPath("accounts", account.getUuid().toString());
        getConfig().set(toPath(path, "nickname"), account.getNickname());
        getConfig().set(toPath(path, "uuid"), account.getUuid().toString());
        for (Currency currency : account.getBalances().keySet()) {
            double balance = account.getBalance(currency);
            getConfig().set(toPath(path, "balances", currency.getUuid()), balance);
        }
        getConfig().set(toPath(path, "payable"), account.canReceiveCurrency());
        saveCurrentState();
    }

    @Override
    public void deleteAccount(Account account) {
        String path = toPath("accounts", account.getUuid());
        getConfig().set(path, null);
        saveCurrentState();
    }

    @Override
    public void createAccount(Account account) {
    }

    @Override
    public ArrayList<Account> getOfflineAccounts() {
        String path = "accounts";
        ArrayList<Account> accounts = new ArrayList<>();
        for (String uuid : getConfig().getConfigurationSection(path).getKeys(false)) {
            Account acc = loadAccount(UUID.fromString(uuid));
            loadBalances(acc);
            accounts.add(acc);
        }
        return accounts;
    }

    private void loadBalances(Account account) {
        String path = toPath("accounts", account.getUuid());
        ConfigurationSection bsection = getConfig().getConfigurationSection(toPath(path, "balances"));
        if (bsection != null) {
            Set<String> balances = bsection.getKeys(false);
            if (!balances.isEmpty()) {
                for (String currency : balances) {
                    String path2 = toPath(path, "balances", currency);
                    double balance = getConfig().getDouble(path2);
                    Currency c = plugin.getCurrencyManager().get(UUID.fromString(currency));
                    if (c != null) {
                        account.modifyBalance(c, balance, false);
                    }
                }
            }
        }
    }

    private void saveCurrentState() {
        String data = null;
        try {
            data = getConfig().saveToString();
        } catch (Exception e) {
            Optional.ofNullable(getConfig())
                    .map(section -> section.getValues(true))
                    .map(Object::toString)
                    .ifPresent(plugin.getLogger()::severe);
            e.printStackTrace();
        }

        if (data != null) {
            String finalData = data;
            Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
                try (FileOutputStream fos = new FileOutputStream(file);
                     OutputStreamWriter writer = new OutputStreamWriter(fos, StandardCharsets.UTF_8)) {
                    writer.write(finalData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public YamlConfiguration getConfig() {
        return configuration;
    }

    public File getFile() {
        return file;
    }

    private static String toPath(Object... nodes) {
        if (nodes.length < 1)
            throw new RuntimeException("At least 1 node must be provided.");

        StringBuilder builder = new StringBuilder(String.valueOf(nodes[0]));
        for (int i = 1; i < nodes.length; i++) {
            String key = String.valueOf(nodes[i]).trim();
            if (key.length() < 1)
                throw new RuntimeException(builder.toString() + " Invalid key " + key);

            builder.append('.');
            builder.append(nodes[i]);
        }

        return builder.toString();
    }
}


