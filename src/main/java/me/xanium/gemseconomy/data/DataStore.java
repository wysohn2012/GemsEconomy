/*
 * Copyright Xanium Development (c) 2013-2018. All Rights Reserved.
 * Any code contained within this document, and any associated APIs with similar branding
 * are the sole property of Xanium Development. Distribution, reproduction, taking snippets or claiming
 * any contents as your own will break the terms of the license, and void any agreements with you, the third party.
 * Thank you.
 */

package me.xanium.gemseconomy.data;

import me.xanium.gemseconomy.GemsEconomy;
import me.xanium.gemseconomy.account.Account;
import me.xanium.gemseconomy.currency.Currency;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

public abstract class DataStore {
    private static final ArrayList<DataStore> methods = new ArrayList<>();

    public final GemsEconomy plugin = GemsEconomy.getInstance();
    private final String name;
    private final boolean topSupported;

    public DataStore(String name, boolean topSupported) {
        this.name = name;
        this.topSupported = topSupported;
    }

    public abstract void initialize();

    public abstract void close();

    public abstract void loadCurrencies();

    /**
     * @param currency
     * @deprecated Not sure what this method does yet. Will investigate more. Possibily can be deleted.
     */
    public abstract void updateCurrencyLocally(Currency currency);

    public abstract void saveCurrency(Currency currency);

    public abstract void deleteCurrency(Currency currency);

    public abstract Map<String, Double> getTopList(Currency currency, int offset, int amount);

    public abstract Account loadAccount(String string);

    public abstract Account loadAccount(UUID uuid);

    //TODO children class must handle this method without blocking the thread
    //currently, YamlStorage is the only class tested to satisfy this requirement
    public abstract void saveAccount(Account account);

    //TODO children class must handle this method without blocking the thread
    //currently, YamlStorage is the only class tested to satisfy this requirement
    public abstract void deleteAccount(Account account);

    //TODO children class must handle this method without blocking the thread
    //currently, YamlStorage is the only class tested to satisfy this requirement
    public abstract void createAccount(Account account);

    public abstract ArrayList<Account> getOfflineAccounts();

    public String getName() {
        return this.name;
    }

    public boolean isTopSupported() {
        return this.topSupported;
    }

    public static DataStore getMethod(String name) {
        //TODO this can be O(1) if we use HashMap, but I don't want to change the method signature atm
        //maybe someone do it in the future (it's not a big deal anyway since elements will be most likely less than 10)
        for (DataStore store : getMethods()) {
            if (store.getName().equalsIgnoreCase(name)) {
                return store;
            }
        }
        return null;
    }

    public static ArrayList<DataStore> getMethods() {
        return methods;
    }
}

