package me.xanium.gemseconomy.currency;

import me.xanium.gemseconomy.GemsEconomy;
import me.xanium.gemseconomy.common.Manager;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class CurrencyManager extends Manager<Currency> {
    private final Map<String, UUID> nameMap;

    private Currency defaultCurrency;

    public CurrencyManager(GemsEconomy plugin) {
        super(plugin);
        this.nameMap = new ConcurrentHashMap<>();
    }

    public boolean currencyExist(String name) {
        return Optional.of(name)
                .map(nameMap::get)
                .map(this::has)
                .orElse(false);
    }

    public Currency getCurrency(String name) {
        return Optional.of(name)
                .map(nameMap::get)
                .map(this::get)
                .orElse(null);
    }

    public Currency getDefaultCurrency() {
        if(defaultCurrency != null){
            return defaultCurrency;
        } else {
            //TODO this is not quite efficient
            defaultCurrency = getAll().stream()
                    .filter(Currency::isDefaultCurrency)
                    .findFirst()
                    .orElse(null);

            return defaultCurrency;
        }
    }

    public void createNewCurrency(String singular, String plural){
        if(currencyExist(singular) || currencyExist(plural)) {
            return;
        }

        Currency currency = new Currency(UUID.randomUUID(), singular, plural);
        currency.setExchangeRate(1.0);
        if(setDefIfFirst && isEmpty()) {
            currency.setDefaultCurrency(true);
        }

        add(currency);
        plugin.getDataStore().saveCurrency(currency);
    }

    public void add(Currency currency) {
        if(has(currency.getUuid()))
            return;

        super.add(currency);
        nameMap.put(currency.getPlural(), currency.getUuid());
    }

    /**
     * @deprecated Using this will delete the instance only, but not the plural name in the nameMap
     * @param uuid
     */
    @Override
    @Deprecated
    public void remove(UUID uuid) {
        throw new RuntimeException("Use remove(Currency)");
    }

    public void remove(Currency currency) {
        super.remove(currency.getUuid());
        nameMap.remove(currency.getPlural());
    }

    public void clearCurrencies(){
        super.clear();
        nameMap.clear();
    }
}
