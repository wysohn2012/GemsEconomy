package me.xanium.gemseconomy.common;

import me.xanium.gemseconomy.GemsEconomy;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A Store for temporary objects.
 * @param <T>
 */
public class Manager<T extends IElement> {
    protected final GemsEconomy plugin;
    private final Map<UUID, T> elements;

    public Manager(GemsEconomy plugin) {
        this.plugin = plugin;
        this.elements = new ConcurrentHashMap<>();
    }

    public T get(UUID uuid) {
        return elements.get(uuid);
    }

    public boolean has(UUID uuid){
        return get(uuid) != null;
    }

    public void remove(UUID uuid) {
        elements.remove(uuid);
    }

    public void add(T account) {
        if (elements.containsKey(account.getUuid()))
            return;

        elements.put(account.getUuid(), account);
    }

    public void clear(){
        elements.clear();
    }

    public boolean isEmpty(){
        return elements.isEmpty();
    }

    public Collection<T> getAll() {
        return Collections.unmodifiableCollection(elements.values());
    }
}
