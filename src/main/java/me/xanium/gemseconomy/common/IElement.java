package me.xanium.gemseconomy.common;

import java.util.UUID;

public interface IElement {
    UUID getUuid();
}
