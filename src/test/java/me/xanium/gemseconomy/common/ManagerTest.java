package me.xanium.gemseconomy.common;

import me.xanium.gemseconomy.GemsEconomy;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;

public class ManagerTest {

    private GemsEconomy mockPluign;
    private TestManager manager;

    @Before
    public void init(){
        mockPluign = mock(GemsEconomy.class);
        manager = new TestManager(mockPluign);
    }

    @Test
    public void testGet() {
        TestValue value = new TestValue(UUID.randomUUID());
        manager.add(value);

        assertEquals(value, manager.get(value.uuid));
    }

    @Test
    public void testRemove() {
        TestValue value = new TestValue(UUID.randomUUID());
        manager.add(value);

        assertEquals(value, manager.get(value.uuid));

        manager.remove(value.uuid);
        assertNull(manager.get(value.uuid));
    }

    @Test
    public void testAdd() {
        TestValue value = new TestValue(UUID.randomUUID());
        manager.add(value);

        assertEquals(value, manager.get(value.uuid));

        TestValue value2 = new TestValue(value.uuid); // duplicate
        manager.add(value2);
        assertEquals(value, manager.get(value.uuid));
    }

    @Test
    public void testClear() {
        TestValue[] values = new TestValue[10];
        for(int i = 0; i < values.length; i++){
            values[i] = new TestValue(UUID.randomUUID());
            manager.add(values[i]);
        }

        for (TestValue value : values) {
            assertEquals(value, manager.get(value.uuid));
        }

        manager.clear();

        for (TestValue value : values) {
            assertNull(manager.get(value.uuid));
        }
    }

    @Test
    public void testGetAll() {

    }

    private static class TestManager extends Manager<TestValue>{
        public TestManager(GemsEconomy plugin) {
            super(plugin);
        }
    }

    private static class TestValue implements IElement{
        private final UUID uuid;

        public TestValue(UUID uuid) {
            this.uuid = uuid;
        }

        @Override
        public UUID getUuid() {
            return uuid;
        }
    }
}