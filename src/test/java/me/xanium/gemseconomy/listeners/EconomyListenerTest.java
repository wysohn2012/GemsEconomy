package me.xanium.gemseconomy.listeners;

import me.xanium.gemseconomy.GemsEconomy;
import me.xanium.gemseconomy.account.Account;
import me.xanium.gemseconomy.account.AccountManager;
import me.xanium.gemseconomy.data.DataStore;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({GemsEconomy.class})
public class EconomyListenerTest {

    private GemsEconomy mockMain;
    private DataStore mockDataStore;

    private EconomyListener listener;
    private Player mockPlayer;
    private String name;
    private UUID uuid;
    private AccountManager mockAccountManager;

    @Before
    public void init() throws Exception {
        mockMain = mock(GemsEconomy.class);
        mockDataStore = mock(DataStore.class);
        mockAccountManager = mock(AccountManager.class);

        mockPlayer = mock(Player.class);
        name = "MockPlayer";
        uuid = UUID.randomUUID();
        when(mockPlayer.getName()).thenReturn(name);
        when(mockPlayer.getUniqueId()).thenReturn(uuid);

        mockStatic(GemsEconomy.class);
        PowerMockito.when(GemsEconomy.class, "getInstance").thenReturn(mockMain);
        when(mockMain.getDataStore()).thenReturn(mockDataStore);
        when(mockMain.getAccountManager()).thenReturn(mockAccountManager);

        listener = new EconomyListener();
    }

    @Test
    public void testOnLogin() throws UnknownHostException {
        InetAddress address = InetAddress.getLocalHost();

        listener.onLogin(new AsyncPlayerPreLoginEvent(name, address, uuid));

        verify(mockDataStore).createAccount(any(Account.class));
        verify(mockDataStore).saveAccount(any(Account.class));
        verify(mockAccountManager).add(any(Account.class));
    }

    @Test
    public void testOnLogin2() throws UnknownHostException {
        InetAddress address = InetAddress.getLocalHost();
        Account mockAccount = mock(Account.class);

        when(mockDataStore.loadAccount(any(UUID.class))).thenReturn(mockAccount);
        listener.onLogin(new AsyncPlayerPreLoginEvent(name, address, uuid));

        verify(mockAccount).setNickname(eq(name));
        verify(mockAccountManager).add(any(Account.class));
    }

    @Test
    public void testOnJoin() {
        listener.onJoin(new PlayerJoinEvent(mockPlayer, "player join mock"));

        verify(mockDataStore).createAccount(any(Account.class));
        verify(mockDataStore).saveAccount(any(Account.class));
        verify(mockAccountManager).add(any(Account.class));
    }

    @Test
    public void testOnJoin2() {
        Account mockAccount = mock(Account.class);

        when(mockAccountManager.get(any(UUID.class))).thenReturn(mockAccount);
        listener.onJoin(new PlayerJoinEvent(mockPlayer, "player join mock"));

        verify(mockAccountManager).add(any(Account.class));
    }

    @Test
    public void testOnJoin3() {
        Account mockAccount = mock(Account.class);

        when(mockDataStore.loadAccount(any(UUID.class))).thenReturn(mockAccount);
        listener.onJoin(new PlayerJoinEvent(mockPlayer, "player join mock"));

        verify(mockAccountManager).add(any(Account.class));
    }

    @Test
    public void testOnQuit() {
        listener.onQuit(new PlayerQuitEvent(mockPlayer, "player quit mock"));

        verify(mockAccountManager).remove(uuid);
    }
}