package me.xanium.gemseconomy.account;

import me.xanium.gemseconomy.GemsEconomy;
import me.xanium.gemseconomy.currency.Currency;
import me.xanium.gemseconomy.data.DataStore;
import me.xanium.gemseconomy.logging.EconomyLogger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({GemsEconomy.class})
public class AccountTest {

    private UUID uuid;
    private String name;
    private Account account;
    private Currency mockCurrency;

    @Before
    public void init() throws Exception {
        uuid = UUID.randomUUID();
        name = "SomePlayer";
        account = new Account(uuid, name);

        mockCurrency = mock(Currency.class);

        GemsEconomy mockPlugin = mock(GemsEconomy.class);
        PowerMockito.mockStatic(GemsEconomy.class);
        PowerMockito.when(GemsEconomy.class, "getInstance").thenReturn(mockPlugin);

        EconomyLogger mockLogger = mock(EconomyLogger.class);
        when(mockPlugin.getEconomyLogger()).thenReturn(mockLogger);

        DataStore mockDataStore = mock(DataStore.class);
        when(mockPlugin.getDataStore()).thenReturn(mockDataStore);
    }

    @Test
    public void withdraw() {
        account.setBalance(mockCurrency, 5000.00);
        assertTrue(account.withdraw(mockCurrency, 4321.12));

        assertEquals(5000.00 - 4321.12, account.getBalance(mockCurrency), 0.001);
    }

    @Test
    public void withdraw2() {
        assertFalse(account.withdraw(mockCurrency, 4321.12));

        assertEquals(0.0, account.getBalance(mockCurrency), 0.001);
    }

    @Test
    public void deposit() {
        assertTrue(account.deposit(mockCurrency, 4323.85));

        assertEquals(4323.85, account.getBalance(mockCurrency), 0.001);
    }

    @Test
    public void deposit2() {
        // not sure what 'canReceiveCurrency' really means, but test it anyway
        account.setCanReceiveCurrency(false);

        assertFalse(account.deposit(mockCurrency, 4323.85));

        assertEquals(0.0, account.getBalance(mockCurrency), 0.001);
    }

    @Test
    public void getBalance() {
        account.deposit(mockCurrency, 8888.43);
        when(mockCurrency.getPlural()).thenReturn("dollar");

        assertEquals(8888.43, account.getBalance("dollar"), 0.001);
    }

    @Test
    public void getBalance2() {
        account.deposit(mockCurrency, 8888.43);
        when(mockCurrency.getPlural()).thenReturn("this");
        when(mockCurrency.getSingular()).thenReturn("that");

        assertEquals(-100.0, account.getBalance("dollar"), 0.001);
    }

    @Test
    public void convert() {
        //TODO
    }
}